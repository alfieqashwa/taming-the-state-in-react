import React, { Component } from 'react';
import './App.css';

const CounterPresenter = props => (
  <div className="App">
    <p>{props.counter}</p>
    <button type="button" onClick={props.onIncrement}>
      ( + )
    </button>
    <button type="button" onClick={props.onDecrement}>
      ( - )
    </button>
  </div>
);

class CounterContainer extends Component {
  constructor(props) {
    super(props);

    this.state = {
      counter: 0
    };
    this.onIncrement = this.onIncrement.bind(this);
    this.onDecrement = this.onDecrement.bind(this);
  }

  onIncrement() {
    this.setState({
      counter: this.state.counter + 1
    });
  }

  onDecrement() {
    this.setState({
      counter: this.state.counter - 1
    });
  }

  render() {
    return (
      <CounterPresenter
        counter={this.state.counter}
        onIncrement={this.onIncrement}
        onDecrement={this.onDecrement}
      />
    );
  }
}
export default CounterContainer;
